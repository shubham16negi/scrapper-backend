let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let keywords = new Schema({
    keyword: String,
    images: [String],
    createDate: {type: Date, default: Date.now}
});

module.exports = mongoose.model('Keywords', keywords);