let config = require('../config');
let mongoose = require('mongoose');


mongoose.connect(config.mongoConfig.getConnectionUrl(), {useMongoClient: true});

let db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function () {
    console.log("Connected");
});
module.exports = db;