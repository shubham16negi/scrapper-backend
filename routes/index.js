let express = require('express');
let router = express.Router();
let request = require('request');
let phantom = require('phantom');
let fs = require('fs');
let path = require('path');
let finalResponse = require('../util/finalResponse');
let KeyWords = require('../db/schema/keywords');


router.post('/getImage', function (req, res, next) {
    let q = req.body.q || "";
    if (q == "") {
        return res.json(finalResponse.error(null, "Query not Found"));
    }
    let url = "http://www.google.co.in/search?q=" + q + "&source=lnms&tbm=isch";
    phantom.create().then(instance => {
        phInstance = instance;
        return instance.createPage();
    }).then(page => {
        page.open(url).then(() => {
            page.includeJs("http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js").then(() => {
                page.evaluateJavaScript("function(){var images = $('.images_table').find('img').map(function() { return this.src; }).get(); return images;}").then(data => {
                    phInstance.exit();
                    res.json(finalResponse.success(data));
                    Promise.all(data.map((node => {
                            return new Promise((resolve, reject) => {
                                request.head(node, (err, res) => {
                                    if (err) {
                                        return reject(err);
                                    }
                                    let i_type = null;
                                    if (res.statusCode === 200) {
                                        i_type = res.headers['content-type'].split('/')[1];
                                        let imageName = new Date().getTime() + "." + i_type;
                                        request(node).pipe(fs.createWriteStream("public/scrapedImages/" + imageName));
                                        resolve(imageName)
                                    } else {
                                        return reject("Not Found")
                                    }
                                })
                            })
                        })
                    )).then((result) => {
                        let keyword = new KeyWords({
                            keyword: q,
                            images: result
                        });
                        keyword.save();
                    }).catch((error) => {
                        return res.json(finalResponse.error(error));
                    });
                })
            })
        });
    }).catch(error => {
        return res.json(finalResponse.error(error, "Error With phantom"));
    });
});

router.get('/keywords', (req, res, next) => {
    KeyWords.find({}, "keyword", (err, docs) => {
        if (err) {
            return res.json(finalResponse.error("error fetching data"))
        } else {
            return res.json(finalResponse.success(docs))
        }
    });
});

router.get('/keyword/:id', (req, res, next) => {
    let id = req.params.id || "";
    if (id === "") {
        return res.json(finalResponse.error(null, "Query not Found"));
    } else {
        KeyWords.findById(id, (err, doc) => {
            if (err) {
                return res.json(finalResponse.error("error fetching data"))
            } else {
                return res.json(finalResponse.success(doc))
            }
        })
    }
});

module.exports = router;
