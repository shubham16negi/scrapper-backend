mongoConfig = {
    host: "localhost",
    port: "27017",
    db: "scrapper",
    user: "",
    password: "",
    getConnectionUrl: function () {
        let user = this.user || "";
        let password = this.password || "";
        if (user === "" || password === "") {
            return "mongodb://" + this.host + ":" + this.port + "/" + this.db;
        } else {
            return "mongodb://" + this.user + ":" + this.password + "@" + this.host + ":" + this.port + "/" + this.db;
        }
    }
};
module.exports = {
    mongoConfig: mongoConfig
};