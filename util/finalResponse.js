finalResponse = {
    success: (data) => {
        return {
            status: 1000,
            body: data,
            message: ""
        }
    },
    error: function (error, message) {
        console.log(error);
        return {
            status: 1001,
            body: null,
            message: message
        }
    }
};
module.exports = finalResponse;